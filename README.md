# 차량, 인공지능솔루션, 엣지서버 연계 Co-simulation 프레임워크 개발

![Untitled__2_](/uploads/9d42c141e64d36949e10f409bd6f53b7/Untitled__2_.png)

- 시뮬레이터-실환경 장치 및 3rd Party 시뮬레이터 연동용 표준 인터페이스 개발

## 개발 개요

### 시뮬레이터-실환경 장치 및 3rd Party 시뮬레이터 연동용 표준 인터페이스 개발

![Untitled__3_](/uploads/7b1dbbdbee7995276e09e5ce26925208/Untitled__3_.png)

- 센서신호 왜곡, 감쇄 반영 표준기반 차량, 엣지 연동 모델링 및 인터페이스
- 도로 노면정보 반영 표준기반 차량, 엣지 연동 모델링 및 인터페이스
- V2X 통신 시스템 연동 프로토콜 모델링 및 인터페이스 기능
- 가상-현실정보 융합 검증용 엣지연동 자율주행 성능평가 시뮬레이터 엔진
- 자율주행 차량과 시뮬레이터 데이터 동기화 및 데이터 융합 기능
- 정밀도로지도 기반 시나리오 정의 및 생성 기능
- 시나리오 기반 통합형 자율주행 시뮬레이션 성능 분석 기능
- 가상-현실정보 연동 가능한 표준기반 시뮬레이터 인터페이스 기능


## 참고 표준 인터페이스

### 자율주행 시뮬레이션 표준 인터페이스: OSI(Open Simulation Interface)

자율주행 시뮬레이터와 실환경 장치 및 3rd Party 연동을 위한 자율주행 표준 인터페이스

**OSI란**

- 자율주행 시뮬레이션 국제 표준 규격
- ISO 23150:2021 Road vehicles - Data communication between sensors and data fusion unit for automated driving functions - Logical interface 준수
- 가상 시나리오 환경에서 자율주행 테스트에 필요한 데이터 인터페이스

**OSI의 주요 데이터 포맷**

- Camera, Lidar, Radar, GT등 다양한 센서 데이터 인터페이스 포맷(SensorData)
- 자율주행 차량 및 주변 차량의 환경에 대한 데이터 인터페이스 포맷(GroundTruth)

**OSI의 동작 흐름**

![Untitled__4_](/uploads/2fc94c4fbc99419007ca86ad6f0f7939/Untitled__4_.png)

**OSI의 현재 범위와 확장 범위**

![Untitled__5_](/uploads/facc5e0ba97fe91f630673be9a021a79/Untitled__5_.png)







